const express = require('express');
const router = express.Router();

module.exports = (connection) => {
  // Ruta para el inicio de sesión
  router.post('/login', (req, res) => {
    // Obtén el número de DPI enviado desde la aplicación móvil
    const { dpi } = req.body;

    // Realiza una consulta SQL para buscar al usuario en la base de datos
    const selectQuery = 'SELECT * FROM usuarios WHERE dpi = ?';
    const valores = [dpi];

    connection.query(selectQuery, valores, (err, result) => {
      if (err) {
        console.error('Error en la consulta:', err);
        res.status(500).json({ message: 'Error en la consulta' });
      } else {
        if (result.length === 0) {
          // No se encontró un usuario con ese DPI
          res.status(404).json({ message: 'Usuario no encontrado' });
        } else {
          // Usuario encontrado, puedes manejar la respuesta aquí si es necesario
          const usuario = result[0];
          console.log('Usuario encontrado:', usuario);
          res.json({ message: 'Inicio de sesión exitoso', usuario });
        }
      }
    });
  });

  return router;
};
